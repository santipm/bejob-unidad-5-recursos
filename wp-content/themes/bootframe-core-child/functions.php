<?php


function login_redirect(){
  global $pagenow;
  if( 'wp-login.php' == $pagenow ) {
    if ( isset( $_POST['wp-submit'] ) ||   // in case of LOGIN
      ( isset($_GET['action']) && $_GET['action']=='logout') ||   // in case of LOGOUT
      ( isset($_GET['checkemail']) && $_GET['checkemail']=='confirm') ||   // in case of LOST PASSWORD
      ( isset($_GET['checkemail']) && $_GET['checkemail']=='registered') ) return;    // in case of REGISTER
    else wp_redirect( home_url('/login')); 
    exit();
  }
} 
add_action('init','login_redirect');


function page_login_func(){

	if(get_the_id() == 9 || get_the_id() == 17 || get_the_id() == 34 || get_the_id() == 42){
    wp_dequeue_style('smartlib_bootstrap' );

    wp_enqueue_style( 'bootstrap-style', get_stylesheet_directory_uri(). '/css/bootstrap.min.css');
    wp_enqueue_style( 'bootstrap-responsive', get_stylesheet_directory_uri(). '/css/bootstrap-responsive.min.css');
    wp_enqueue_style( 'style-child', get_stylesheet_directory_uri(). '/css/style.css');
    wp_enqueue_style( 'style-responsive', get_stylesheet_directory_uri(). '/css/style-responsive.css');

     wp_enqueue_style( 'fonts-google', 'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext');
   
	}

}

add_action('wp_enqueue_scripts', 'page_login_func',99,1 );


function hide_adminbar(){
  if(!current_user_can('manage_options' )){ 
      show_admin_bar(false);
      echo '<style type="text/css">
      body { margin-top: -32px !important; }
      </style>';
    }
}

add_action('wp_head','hide_adminbar');


function comprobar_si_existe($user_id,$not_id){
  global $wpdb;

  $wpdb->get_results( "SELECT * FROM `wp_notificaciones_usuarios` WHERE `user_id` =". $user_id." AND `notificacion_id` =". $not_id); 
  
  if($wpdb->num_rows==0){
    return TRUE;
  }else{
    return FALSE;
  }
}


// Register Custom Post Type
function recurso_post_type() {

  $labels = array(
    'name'                  => 'Recursos',
    'singular_name'         => 'Recurso',
    'menu_name'             => 'Recursos',
    'name_admin_bar'        => 'Recursos admin bar',
    'archives'              => 'Item Archives',
    'parent_item_colon'     => 'Parent Item:',
    'all_items'             => __('Todos los recursos'),
    'add_new_item'          => 'Add New Item',
    'add_new'               => __('Añadir Recurso'),
    'new_item'              => 'New Item',
    'edit_item'             => 'Edit Item',
    'update_item'           => 'Update Item',
    'view_item'             => 'View Item',
    'search_items'          => 'Search Item',
    'not_found'             => 'Not found',
    'not_found_in_trash'    => 'Not found in Trash',
    'featured_image'        => 'Featured Image',
    'set_featured_image'    => 'Set featured image',
    'remove_featured_image' => 'Remove featured image',
    'use_featured_image'    => 'Use as featured image',
    'insert_into_item'      => 'Insert into item',
    'uploaded_to_this_item' => 'Uploaded to this item',
    'items_list'            => 'Items list',
    'items_list_navigation' => 'Items list navigation',
    'filter_items_list'     => 'Filter items list',
  );

  $args = array(
    'labels'                => $labels,
    'description'           => 'Recursos',
    'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail'),
    'taxonomies'            => array( 'post_tag' ),
    'menu_icon'             => 'dashicons-admin-site',
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'can_export'            => true,
    'has_archive'           => false,   
    'exclude_from_search'   => true,
    'publicly_queryable'    => false,
    'capability_type'       => 'page',
  );
  register_post_type( 'recursos', $args );

}
add_action( 'init', 'recurso_post_type',99 );

function subir_imagen_post($id){

  //Algoritmo subir imágenes
      $upload_dir_var = wp_upload_dir(); // obtenemos la ubicación de los archivos subidos en nuestro WordPress
      $upload_dir = $upload_dir_var['path']; // obtenemos el path absoluto de la carpeta de archivos subidos

      $filename = basename($_FILES['imagen']['name']); // obtenemos el nombre del archivo subido con el form
      $filename = trim($filename); // eliminamos posibles espacios antes y después del nombre de archivo
      $filename = ereg_replace(" ", "-", $filename); // eliminamos posibles espacios intersticiales en el nombre de archivo

      $typefile = $_FILES['imagen']['type']; // obtenemos el tipo de archivo (JPG, PNG...)

      $uploaddir = realpath($upload_dir); // nos aseguramos de que el path de la carpeta de archivos subidos es absoluto
      // esto es importante, si no es absoluto no funcionará
      $uploadfile = $uploaddir.'/'.$filename; // formamos el nombre definitivo que tendrá el archivo

      $slugname = preg_replace('/\.[^.]+$/', '', basename($uploadfile)); // este es el nombre que tendrá la imagen en la base de datos de imágenes de WordPress

      if ( file_exists($uploadfile) ) { // si un archivo con el mismo nombre ya existe se añade un sufijo al nombre

              $count = "0";
              while ( file_exists($uploadfile) ) {
              $count++;
              if ( $typefile == 'image/jpeg' ) { $exten = 'jpg'; }
              elseif ( $typefile == 'image/png' ) { $exten = 'png'; }
              elseif ( $typefile == 'image/gif' ) { $exten = 'gif'; }
              $uploadfile = $uploaddir.'/'.$slugname.'-'.$count.'.'.$exten;
              }
      } // fin if file_exists

      if (move_uploaded_file($_FILES['imagen']['tmp_name'], $uploadfile)){
              // aquí ejecutaremos el código para insertar la imagen en la base de datos
              $post_id = $id; // el identificador del post o página al que queremos asociar la imagen
              $slugname = preg_replace('/\.[^.]+$/', '', basename($uploadfile)); // tras la reubicación del archivo definimos definitivamente su nombre
              $attachment = array(
            'post_mime_type' => $typefile, // tipo de archivo
            'post_title' => $slugname, // el nombre del archivo en la Libreria de medios
            'post_content' => '', // contenido extra asociado a la imagen
            'post_status' => 'inherit'
            );

           $attach_id = wp_insert_attachment( $attachment, $uploadfile, $post_id );
             // se debe incluir el archivo image.php
             // para que la función wp_generate_attachment_metadata() funcione
           require_once(ABSPATH . "wp-admin" . '/includes/image.php');

           $attach_data = wp_generate_attachment_metadata( $attach_id, $uploadfile );
           wp_update_attachment_metadata( $attach_id,  $attach_data );
           set_post_thumbnail( $post_id, $attach_id );
            }
      //Fin de Algoritmo
}
