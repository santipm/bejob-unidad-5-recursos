<?php
/**
 * Template name: Panel
 */?>

 <?php 
 	if(!is_user_logged_in()){
 		header('Location:'.home_url('/login'));
 		exit();
 	}
 	//Obtener usuario con la sesión activa
 	$user = wp_get_current_user();


  if($_SERVER['REQUEST_METHOD'] == 'POST'):

        $comentario =$_POST['comentario'];

        $resultado = $wpdb->insert(
            'wp_notificaciones',
            array(
                    'user_id' => get_current_user_id(),
                    'texto' => $comentario,
                    'fecha'	=> date('Y-m-d H:i:s')
                )
        );

        if($resultado):
        	$users = get_users('role=Subscriber');
          $emails = array();
        	foreach ($users as $key => $value) {
        		$emails[] = $value->user_email;

        	}

        	wp_mail( $emails, 'Nueva notificación', $comentario );
        endif;
  else:
  	if(isset($_GET['parametro']) && $_GET['parametro'] == 'like'):

  	$wpdb->get_results( "SELECT * FROM `wp_notificaciones_usuarios` WHERE `user_id` =". $user->ID." AND `notificacion_id` =". $_GET['id_not']); 
  	$id_not = $_GET['id_not'];
  	if(comprobar_si_existe($user->ID,$_GET['id_not'])){
  		
  		$wpdb->query($wpdb->prepare("UPDATE wp_notificaciones SET interesa=interesa+1 WHERE id=%d", $_GET['id_not']));
  		/*Existe otro métopdo para actualización de código
  			$wpdb->update( 
								'table', 
								array( 
									'column1' => 'value1',	// string
									'column2' => 'value2'	// integer (number) 
								), 
								array( 'ID' => 1 ), 
								array( 
									'%s',	// value1
									'%d'	// value2
								), 
								array( '%d' ) 
							);*/

					$wpdb->insert(
            'wp_notificaciones_usuarios',
            array(
                    'user_id' => get_current_user_id(),
                    'notificacion_id' => $_GET['id_not'],
                )
            );
				}
  	
  	endif;
      
  endif;

    $notificaciones = $wpdb->get_results("SELECT * FROM wp_notificaciones ORDER BY id DESC");
?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->	
	<?php wp_head(); ?>	
</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="<?php echo home_url('/panel-principal');?>"><span><?php echo bloginfo('name'); ?> </span></a>
								
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">

						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i> <?php echo $user->display_name;?>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
 									<span>Account Settings</span>
								</li>
								<li><a href="<?php echo home_url('/perfil');?>"><i class="halflings-icon user"></i> Profile</a></li>
								<li><a href="<?php echo wp_logout_url() ; ?>"><i class="halflings-icon off"></i> Logout</a></li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid-full">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<div id="sidebar-left" class="span2">
				<div class="nav-collapse sidebar-nav">
				<?php $args= array(
										'menu' 			 => 'Menú panel usuario',
										'menu_class' => 'nav nav-tabs nav-stacked main-menu',
										'container'	 =>	'ul',
										'link_before'=>	'<i class="fa fa-circle"></i><span class="hidden-tablet"> ',
										'link_after' =>	'</span>',
														);

					wp_nav_menu( $args );

				if(current_user_can('manage_options' )){
					$args2= array('menu' 				=> 'Menú panel admin',
												'menu_class' 	=> 'nav nav-tabs nav-stacked main-menu',
												'container'		=>'ul',
												'link_before'	=>'<i class="fa fa-circle"></i><span class="hidden-tablet"> ',
												'link_after'	=>'</span>'
														);
					wp_nav_menu( $args2 );
				}

				?>
					
				</div>
			</div>
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<!-- start: Content -->
			<div id="content" class="span10">
			<?php if($resultado){ ?>
				<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<strong>Notificación correcta</strong>
						</div>
			<?php } ?>
			<div class="row-fluid">
				<h2>Listado de notificaciones</h2>
				<ul class="chat">
					<?php 
						foreach ($notificaciones as $not) {
							//Obtengo más datos del usuarios con ID
							$usuario = get_user_by('ID', $not->user_id)
					?>
						<li class="left">
							<?php echo get_avatar( $not->user_id);?>
							<span class="message"><span class="arrow"></span>
								<span class="from"><?php echo $usuario->user_nicename; ?> </span>
								<span class="time"><?php echo $not->fecha; ?></span>
								<span class="text">
									<?php echo $not->texto; ?>
								</span>
								<p>
									:-)
								<?php
									
									if(comprobar_si_existe($not->user_id,$not->id)){?>
										<a href="<?php echo home_url('panel-principal?parametro=like&id_not='. $not->id);?>">
									<?php } ?>
										<i  class="fa fa-thumbs-o-up fa-2x text-info"></i>
									</a>
									:-(
									<i  class="fa fa-thumbs-o-down fa-2x text-info"></i>
								</p>
								<span class="text">
									Votos a favor: <?php echo $not->interesa;?>
								<span>
								<span class="text">
									Votos encontra: <?php echo $not->no_interesa;?>
								<span>
							</span>	                                  
						</li>

					<?php } ?>

						</ul>
			</div>
			<div class="row-fluid">
			<h2> Notificaciones </h2>
				<form class="replyForm" method="post" action="">

					<fieldset>
						<textarea tabindex="3" class="input-xlarge span12" id="message" name="comentario" rows="12" placeholder="Click here to reply" required></textarea>

						<div class="actions">
							
							<button tabindex="3" type="submit" class="btn btn-success">Enviar notificación</button>
							
						</div>

					</fieldset>

				</form>
			</div>
       

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>
	
	<div class="clearfix"></div>
	
	<footer>

		<p>
			<span style="text-align:left;float:left">&copy; 2013 <a href="http://jiji262.github.io/Bootstrap_Metro_Dashboard/" alt="Bootstrap_Metro_Dashboard">Bootstrap Metro Dashboard</a></span>
			
		</p>
	<?php wp_footer(); ?>
	</footer>
	

	
</body>
</html>
